import { UserResponseInterface } from 'src/app/config/interfaces/apis-response/user-response.interface';
import { UserInterface } from 'src/app/config/interfaces/user.interface';

export class UserMapper {
  public userSingleMapper(userInfo: UserResponseInterface): UserInterface {
    return {
      country: userInfo.country,
      displayName: userInfo.display_name,
      email: userInfo.email,
      id: userInfo.id,
      images: [
        {
          url: userInfo.images[0].url,
        },
      ],
    };
  }
}
