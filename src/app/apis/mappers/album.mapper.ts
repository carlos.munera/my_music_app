import { AlbumInterface } from 'src/app/config/interfaces/album.interface';
import { AlbumResponseInterface } from 'src/app/config/interfaces/apis-response/album-response.interface';
import { TrackResponseInterface } from 'src/app/config/interfaces/apis-response/track-response.interface';
import { TrackInterface } from 'src/app/config/interfaces/track.interface';

export class AlbumMapper {
  public albumMapper(albumInfo: AlbumResponseInterface): AlbumInterface {
    return {
      artists: [
        {
          name: albumInfo.artists[0].name,
        },
      ],
      id: albumInfo.id,
      images: [
        {
          url: albumInfo.images[0].url,
        },
      ],
      name: albumInfo.name,
      totalTracks: albumInfo.total_tracks,
      tracks: {
        items: !albumInfo.tracks
          ? <TrackInterface[]>(<unknown>undefined)
          : albumInfo.tracks.items.map((item) => {
              return this.trackMapper(item);
            }),
      },
      favorite: false,
    };
  }

  public trackMapper(trackInfo: TrackResponseInterface): TrackInterface {
    return {
      artists: [
        {
          name: trackInfo.artists[0].name,
        },
      ],
      discNumber: trackInfo.disc_number,
      durationMs: trackInfo.duration_ms,
      id: trackInfo.id,
      name: trackInfo.name,
      trackNumber: trackInfo.track_number,
    };
  }
}
