import { Injectable } from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { ApiRoutes } from '@config/constants/api-routes';
import { UserResponseInterface } from '@config/interfaces/apis-response/user-response.interface';
import { ParamsMethodInterface } from '@config/interfaces/params-method.interface';
import { UserInterface } from '@config/interfaces/user.interface';

import { HttpService } from '@services/http.service';
import { UserMapper } from './mappers/user.mapper';

@Injectable({
  providedIn: 'root',
})
export class UserApi {
  constructor(private httpService: HttpService) {}

  public async getProfile(): Promise<UserInterface> {
    let userInfo = <UserInterface>(<unknown>undefined);

    await lastValueFrom(
      this.httpService.get<UserResponseInterface>({
        url: ApiRoutes.USER_PROFILE,
        typeAuth: 'bearer',
      } as ParamsMethodInterface)
    )
      .then((data) => {
        userInfo = new UserMapper().userSingleMapper(data);
      })
      .catch((error: any) => {
        console.log(error);
      });

    return userInfo;
  }
}
