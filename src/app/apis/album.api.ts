import { Injectable } from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { ApiRoutes } from '@config/constants/api-routes';
import { AlbumInterface } from '@config/interfaces/album.interface';
import { AlbumResponseInterface } from '@config/interfaces/apis-response/album-response.interface';

import { HttpService } from '@services/http.service';
import { AlbumMapper } from './mappers/album.mapper';
import { HomeResponseInterface } from '@config/interfaces/apis-response/home-response.interface';
import { FavoriteResponseInterface } from '@config/interfaces/apis-response/favorites-response.interface';
import { ParamsMethodInterface } from '@config/interfaces/params-method.interface';

@Injectable({
  providedIn: 'root',
})
export class AlbumApi {
  constructor(private httpService: HttpService) {}

  public async getAlbum(fromFavorite: boolean): Promise<AlbumInterface[]> {
    const routeApi = fromFavorite
      ? ApiRoutes.FAVORITE_ALBUMS
      : ApiRoutes.NEW_ALBUMS;

    let albumList = <AlbumInterface[]>(<unknown>undefined);

    await lastValueFrom(
      this.httpService.get<HomeResponseInterface>({
        url: routeApi,
        typeAuth: 'bearer',
      } as ParamsMethodInterface)
    )
      .then((data) => {
        albumList = data.albums.items.map((item: AlbumResponseInterface) => {
          return new AlbumMapper().albumMapper(item);
        });
      })
      .catch((error: any) => {
        console.log(error);
      });

    return albumList;
  }

  public async getFavoriteAlbum(
    fromFavorite: boolean
  ): Promise<AlbumInterface[]> {
    const routeApi = fromFavorite
      ? ApiRoutes.FAVORITE_ALBUMS
      : ApiRoutes.NEW_ALBUMS;

    let albumList = <AlbumInterface[]>(<unknown>undefined);

    await lastValueFrom(
      this.httpService.get<FavoriteResponseInterface>({
        url: routeApi,
        typeAuth: 'bearer',
      } as ParamsMethodInterface)
    )
      .then((data) => {
        albumList = data.items.album.map((item: AlbumResponseInterface) => {
          return new AlbumMapper().albumMapper(item);
        });
      })
      .catch((error: any) => {
        console.log(error);
      });

    return albumList;
  }
}
