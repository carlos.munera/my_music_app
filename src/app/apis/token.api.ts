import { Injectable } from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { ApiRoutes } from '@config/constants/api-routes';
import { StorageType } from '@config/constants/storage-type';
import { ParamsMethodInterface } from '@config/interfaces/params-method.interface';
import { TokenInterface } from '@config/interfaces/token.interface';

import { HttpService } from '@services/http.service';
import { LocalStorageService } from '@services/local-storage.service';

@Injectable({
  providedIn: 'root',
})
export class AuthApi {
  constructor(
    private httpService: HttpService,
    private localStorageService: LocalStorageService
  ) {}

  public async getToken(
    params: unknown,
    authToken?: string
  ): Promise<TokenInterface> {
    let tokenInfo = <TokenInterface>(<unknown>undefined);

    await lastValueFrom(
      this.httpService.post<any>({
        url: ApiRoutes.TOKEN,
        body: params,
        typeAuth: 'basic',
        authToken,
      } as ParamsMethodInterface)
    ).then((data) => {
      this.localStorageService.set(StorageType.ACCESS_TOKEN, data.access_token);

      tokenInfo = data;
    });

    return tokenInfo;
  }
}
