import { albumReducer } from '../state/albums/albums.reducer';
import { menuReducer } from '../state/menu/menu.reducer';
import { userReducer } from '../state/user/user.reducer';

export const reducers = {
  user: userReducer,
  albums: albumReducer,
  menuPath: menuReducer,
};
