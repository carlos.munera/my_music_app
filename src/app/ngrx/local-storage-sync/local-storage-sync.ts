import { ActionReducer, MetaReducer } from '@ngrx/store';
import { localStorageSync } from 'ngrx-store-localstorage';

const persistList: string[] = ['user', 'albums', 'menuPath'];

function localStorageSyncReducer(
  reducer: ActionReducer<any>
): ActionReducer<any, any> {
  return localStorageSync({
    keys: persistList,
    rehydrate: true,
    storageKeySerializer: (key) => `local_${key}`,
  })(reducer);
}

export const metaReducers: Array<MetaReducer<any, any>> = [
  localStorageSyncReducer,
];
