import { AlbumInterface } from '../../config/interfaces/album.interface';
import { UserInterface } from '../../config/interfaces/user.interface';

export interface AppState {
  user: Readonly<UserInterface>;
  albums: Readonly<AlbumInterface[]>;
  menuPath: Readonly<string>;
}
