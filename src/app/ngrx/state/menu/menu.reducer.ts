import { createReducer, on } from '@ngrx/store';
import { getMenuPath } from './menu.actions';

export const initialState: Readonly<string> = '';

export const menuReducer = createReducer(
  initialState,
  on(getMenuPath, (state: Readonly<string>, { menuPath }) => menuPath)
);
