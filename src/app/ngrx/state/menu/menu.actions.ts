import { createAction, props } from '@ngrx/store';

export const getMenuPath = createAction(
  '[Menu Path] Get Path Menu',
  props<{ menuPath: Readonly<string> }>()
);
