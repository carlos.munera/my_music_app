import { createFeatureSelector } from '@ngrx/store';

export const selectMenuPath =
  createFeatureSelector<Readonly<string>>('menuPath');
