import { createReducer, on } from '@ngrx/store';
import { UserInterface } from 'src/app/config/interfaces/user.interface';
import { getUserProfile } from './user.actions';

export const initialState: Readonly<UserInterface> = <UserInterface>(
  (<unknown>undefined)
);

export const userReducer = createReducer(
  initialState,
  on(getUserProfile, (state: UserInterface, { userProfile }) => userProfile)
);
