import { createAction, props } from '@ngrx/store';
import { UserInterface } from '@config/interfaces/user.interface';

export const getUserProfile = createAction(
  '[User Profile] Profile',
  props<{ userProfile: Readonly<UserInterface> }>()
);
