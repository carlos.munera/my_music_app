import { createFeatureSelector } from '@ngrx/store';
import { UserInterface } from '@config/interfaces/user.interface';

export const selectProfile =
  createFeatureSelector<Readonly<UserInterface>>('user');
