import { createReducer, on } from '@ngrx/store';
import { AlbumInterface } from 'src/app/config/interfaces/album.interface';
import {
  getAlbumList,
  toggleFavoriteIds,
  toggleFavorite,
} from './albums.actions';

export const initialState: Readonly<AlbumInterface[]> = [];

export const albumReducer = createReducer(
  initialState,
  on(
    getAlbumList,
    (state: Readonly<AlbumInterface[]>, { albumList }) => albumList
  ),
  on(toggleFavoriteIds, toggleFavorite)
);
