import { createAction, props } from '@ngrx/store';
import { AlbumInterface } from 'src/app/config/interfaces/album.interface';

export const getAlbumList = createAction(
  '[Album List] Get All Albums',
  props<{ albumList: Readonly<AlbumInterface[]> }>()
);

export const toggleFavoriteIds = createAction(
  '[Album/Toggle Favorites] Toggle Favorites',
  props<{ albumId: string }>()
);

export const toggleFavorite = (
  state: Readonly<AlbumInterface[]>,
  action: { albumId: string }
): AlbumInterface[] => {
  const { albumId } = action;

  return state.map((album) => {
    return {
      ...album,
      favorite: album.id === albumId ? !album.favorite : album.favorite,
    };
  });
};
