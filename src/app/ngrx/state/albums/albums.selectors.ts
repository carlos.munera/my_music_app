import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AlbumInterface } from 'src/app/config/interfaces/album.interface';

export const selectAlbums =
  createFeatureSelector<Readonly<AlbumInterface[]>>('albums');

export const selectFavoriteAlbums = createSelector(selectAlbums, (state) => {
  return state.filter((album) => album.favorite === true);
});
