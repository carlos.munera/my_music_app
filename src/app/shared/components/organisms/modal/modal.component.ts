import { Component, Input, OnInit } from '@angular/core';
import { GlobalService } from '@src/app/services/global.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  @Input() modalMessage = 'Cargando...';
  @Input() modalClass = 'loading';

  constructor(private globalService: GlobalService) {}

  ngOnInit(): void {}

  public dismissModal(): void {
    this.globalService.$openModal.emit(false);
  }
}
