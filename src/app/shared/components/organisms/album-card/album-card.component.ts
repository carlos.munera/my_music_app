import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AlbumInterface } from 'src/app/config/interfaces/album.interface';

@Component({
  selector: 'app-album-card',
  templateUrl: './album-card.component.html',
  styleUrls: ['./album-card.component.scss'],
})
export class AlbumCardComponent implements OnInit {
  @Input() albumCardInfo: Readonly<AlbumInterface[] | null> = [];
  @Input() albumTitle = '';
  @Input() albumSubtitle = '';

  @Output() toggleFav = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {}
}
