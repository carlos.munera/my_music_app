import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { UserInterface } from '@config/interfaces/user.interface';
import { selectProfile } from '@ngrxStore/state/user/user.selectors';
import { GlobalService } from '@services/global.service';
import { LocalStorageService } from '@services/local-storage.service';
import { getMenuPath } from '@ngrxStore/state/menu/menu.actions';
import { selectMenuPath } from '@ngrxStore/state/menu/menu.selectors';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {
  @Input() toolbarClass = '';
  @Input() fromFooter = false;

  public segmentUrl = new Subscription();

  profile$: Observable<Readonly<UserInterface>>;
  menuPath$: Observable<Readonly<string>>;

  constructor(
    private localStorageService: LocalStorageService,
    private activateRoute: ActivatedRoute,
    private globalService: GlobalService
  ) {
    this.profile$ = this.globalService.store.select(selectProfile);
    this.menuPath$ = this.globalService.store.select(selectMenuPath);
  }

  ngOnInit(): void {
    this.segmentUrl = this.activateRoute.pathFromRoot[1].url.subscribe(
      (val) => {
        const menuPath = val[0].path;
        this.globalService.store.dispatch(getMenuPath({ menuPath }));
      }
    );
  }

  ngOnDestroy(): void {
    this.segmentUrl.unsubscribe();
  }

  public logout() {
    this.localStorageService.delete();
    this.globalService.goTo('login');
  }

  public goTo(redirectTo: string) {
    this.globalService.goTo(redirectTo);
  }
}
