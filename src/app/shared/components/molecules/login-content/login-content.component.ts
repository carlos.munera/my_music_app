import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-login-content',
  templateUrl: './login-content.component.html',
  styleUrls: ['./login-content.component.scss'],
})
export class LoginContentComponent implements OnInit {
  @Input() showModal = false;
  @Output() onclick = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}

  public onclickBtn() {
    this.onclick.emit();
  }
}
