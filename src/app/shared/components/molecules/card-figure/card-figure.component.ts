import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AlbumInterface } from 'src/app/config/interfaces/album.interface';

@Component({
  selector: 'app-card-figure',
  templateUrl: './card-figure.component.html',
  styleUrls: ['./card-figure.component.scss'],
})
export class CardFigureComponent implements OnInit {
  @Input() album: AlbumInterface = <AlbumInterface>(<unknown>undefined);
  @Output() onAdd = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}
}
