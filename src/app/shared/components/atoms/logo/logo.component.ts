import { Component, Input, OnInit } from '@angular/core';
import { GlobalService } from '@services/global.service';

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss'],
})
export class LogoComponent implements OnInit {
  @Input() logoClass = '';

  constructor(private globalService: GlobalService) {}

  ngOnInit(): void {}

  public goTo(redirectTo: string) {
    this.globalService.goTo(redirectTo);
  }
}
