import { Component, Input, OnInit } from '@angular/core';
import { ObjectInterface } from 'src/app/config/interfaces/object.interface';

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
})
export class IconComponent implements OnInit {
  @Input() icon = '';
  @Input() isFavorite = false;
  @Input() iconClass = '';

  constructor() {}

  ngOnInit(): void {}

  get classes() {
    const cssClasses: ObjectInterface = {
      fa: true,
    };
    cssClasses[this.icon] = true;
    cssClasses['album__favorite--active'] = this.isFavorite;
    return cssClasses;
  }
}
