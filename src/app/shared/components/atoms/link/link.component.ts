import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.scss'],
})
export class LinkComponent implements OnInit {
  @Input() goTo = '';
  @Input() linkNgClass = <Record<string, unknown>>(<unknown>undefined);
  @Output() clickEvent = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {}

  public onClick(): void {
    this.clickEvent.emit(this.goTo);
  }
}
