import { NgModule } from '@angular/core';
import { PipesModule } from './pipes/pipes.module';
import { LogoutBtnComponent } from './components/atoms/logout-btn/logout-btn.component';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/organisms/header/header.component';
import { LogoComponent } from './components/atoms/logo/logo.component';
import { IconComponent } from './components/atoms/icon/icon.component';
import { LinkComponent } from './components/atoms/link/link.component';
import { BasePageComponent } from './components/template/base-page/base-page.component';
import { AlbumCardComponent } from './components/organisms/album-card/album-card.component';
import { ModalComponent } from './components/organisms/modal/modal.component';
import { ImageComponent } from './components/atoms/image/image.component';
import { CardFigureComponent } from './components/molecules/card-figure/card-figure.component';
import { LabelTracksComponent } from './components/atoms/label-tracks/label-tracks.component';
import { ButtonComponent } from './components/atoms/button/button.component';
import { LoginContentComponent } from './components/molecules/login-content/login-content.component';
import { FooterComponent } from './components/organisms/footer/footer.component';
import { ToolbarComponent } from './components/molecules/toolbar/toolbar.component';

@NgModule({
  declarations: [
    LogoutBtnComponent,
    HeaderComponent,
    LogoComponent,
    IconComponent,
    LinkComponent,
    BasePageComponent,
    AlbumCardComponent,
    ModalComponent,
    ImageComponent,
    CardFigureComponent,
    LabelTracksComponent,
    ButtonComponent,
    LoginContentComponent,
    FooterComponent,
    ToolbarComponent,
  ],
  imports: [CommonModule, PipesModule],
  exports: [
    HeaderComponent,
    ModalComponent,
    BasePageComponent,
    AlbumCardComponent,
    LoginContentComponent,
    FooterComponent,
  ],
})
export class SharedModule {}
