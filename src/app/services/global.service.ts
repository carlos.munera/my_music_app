import { EventEmitter, Injectable } from '@angular/core';
import { DefaultProjectorFn, MemoizedSelector, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { toggleFavoriteIds } from '@ngrxStore/state/albums/albums.actions';
import { AppState } from '@ngrxStore/state/app-state.interface';
import { Router } from '@angular/router';
import { AppRoutes } from '../config/constants/app-routes';

@Injectable({
  providedIn: 'root',
})
export class GlobalService {
  public $openModal = new EventEmitter<boolean>();

  constructor(public store: Store<AppState>, private router: Router) {}

  /* ---------------------------------------------------------------- */
  /**
   *  ngrx store operations
   */

  public getSelectorSingle<T>(
    selectorType: MemoizedSelector<
      object,
      Readonly<T>,
      DefaultProjectorFn<Readonly<T>>
    >
  ): Observable<Readonly<T>> {
    return this.store.select(selectorType);
  }

  public getSelectorList<T>(
    selectorType: MemoizedSelector<
      object,
      Readonly<T[]>,
      DefaultProjectorFn<Readonly<T[]>>
    >
  ): Observable<Readonly<T[]>> {
    return this.store.select(selectorType);
  }

  toggleFavorite(albumId: string) {
    this.store.dispatch(toggleFavoriteIds({ albumId }));
  }

  /* ---------------------------------------------------------------- */

  public objectToQueryParams(obj: Record<string, unknown>): string {
    return Object.keys(obj)
      .map((key) => key + '=' + obj[key])
      .join('&');
  }

  public generateRandomString(length: number): string {
    var text = '';
    var possible =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < length; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
  }

  public goTo(redirectTo: string) {
    this.router.navigate([AppRoutes[redirectTo]]);
  }
}
