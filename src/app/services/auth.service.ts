import { Injectable } from '@angular/core';
import { StorageType } from '../config/constants/storage-type';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public isAuthorized(): boolean {
    const token = localStorage.getItem(StorageType.ACCESS_TOKEN);

    return token ? true : false;
  }
}
