import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  public async get(key: string): Promise<unknown> {
    try {
      return await localStorage.getItem(key);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  public async set(key: string, value: string): Promise<unknown> {
    try {
      return await localStorage.setItem(key, value);
    } catch (error) {
      return Promise.reject(error);
    }
  }

  public async delete(key?: string): Promise<unknown> {
    try {
      if (key) {
        return await localStorage.removeItem(key);
      }

      return await localStorage.clear();
    } catch (error) {
      return Promise.reject(error);
    }
  }
}
