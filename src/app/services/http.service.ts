import { Injectable } from '@angular/core';
import { catchError, Observable, of } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { StorageType } from '@config/constants/storage-type';
import { ParamsMethodInterface } from '@config/interfaces/params-method.interface';
import { prefixAuthorization } from '@config/constants/prefix-authorization';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  private env = environment;

  constructor(private http: HttpClient) {}

  public post<T>({
    url,
    body,
    typeAuth,
    authToken,
    responseType = 'json',
  }: ParamsMethodInterface): Observable<T> {
    return this.http
      .post<T>(
        this.env.host[authToken ? 'MUSIC_APP_AUTH' : 'MUSIC_APP_API'] + url,
        body,
        {
          headers: this.getHeaders(typeAuth, authToken),
          responseType: responseType as 'json',
        }
      )
      .pipe(
        catchError(this.handleError<T>('PostMethod', <T>(<unknown>undefined)))
      );
  }

  public get<T>({ url, typeAuth }: ParamsMethodInterface): Observable<T> {
    return this.http
      .get<T>(this.env.host.MUSIC_APP_API + url, {
        headers: this.getHeaders(typeAuth),
      })
      .pipe(
        catchError(this.handleError<T>('getMethod', <T>(<unknown>undefined)))
      );
  }

  public put({ url, body, typeAuth }: ParamsMethodInterface): Observable<any> {
    return this.http
      .put(this.env.host.MUSIC_APP_API + url, body, {
        headers: this.getHeaders(typeAuth),
      })
      .pipe(catchError(this.handleError<any>('UpdateMethod')));
  }

  public delete({
    url,
    body,
    typeAuth,
  }: ParamsMethodInterface): Observable<any> {
    return this.http
      .delete(this.env.host.MUSIC_APP_API + url, {
        headers: this.getHeaders(typeAuth),
        body: body,
      })
      .pipe(catchError(this.handleError<any>('DeleteMethod')));
  }

  private getHeaders(
    typeAuth: string,
    authToken?: string
  ): Record<string, string> {
    if (localStorage.getItem(StorageType.ACCESS_TOKEN) || authToken) {
      return {
        Authorization:
          prefixAuthorization[typeAuth]['prefix'] +
          (authToken
            ? authToken
            : localStorage.getItem(StorageType.ACCESS_TOKEN)),
        'Content-type': prefixAuthorization[typeAuth]['contentType'],
      };
    }

    return { Authorization: '' };
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: HttpErrorResponse): Observable<T> => {
      let errorMessage = 'Unknown error!';
      if (error.error instanceof ErrorEvent) {
        // Client-side errors
        errorMessage = `${operation} Error: ${error.error.message}`;
      } else {
        // Server-side errors
        errorMessage = `${operation} Error Code: ${error.status}\nMessage: ${error.message}`;
      }

      window.alert(errorMessage);

      // return throwError(() => new Error(errorMessage));
      return of(result as T);
    };
  }
}
