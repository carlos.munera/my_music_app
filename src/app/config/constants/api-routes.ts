export enum ApiRoutes {
  TOKEN = '/api/token',
  AUTHORIZE = '/authorize',
  USER_PROFILE = '/v1/me',
  TRACKS = 'v1/tracks',
  ALBUMS = 'v1/albums',
  FAVORITE_ALBUMS = '/v1/me/albums',
  NEW_ALBUMS = '/v1/browse/new-releases',
  FAVORITE_TRACKS = '/v1/me/tracks',
}
