export enum StorageType {
  USER_DATA = 'user_data',
  ACCESS_TOKEN = 'access_token',
  EXPIRES_AT = 'expires_at',
}
