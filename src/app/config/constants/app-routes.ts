import { ObjectInterface } from '../interfaces/object.interface';

export const AppRoutes: ObjectInterface = {
  favorites: 'favorites/library',
  home: 'home/list',
  login: 'account/login',
  profile: 'account/profile',
};
