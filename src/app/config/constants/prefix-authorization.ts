import { ObjectKeyInterface } from '../interfaces/object-key.interface';

export const prefixAuthorization: ObjectKeyInterface = {
  basic: {
    prefix: 'Basic ',
    contentType: 'application/x-www-form-urlencoded',
  },
  bearer: {
    prefix: 'Bearer ',
    contentType: 'application/json',
  },
};
