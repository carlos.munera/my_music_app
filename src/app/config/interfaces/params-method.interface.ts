export interface ParamsMethodInterface {
  url: string;
  body: unknown;
  typeAuth: string;
  authToken?: string;
  responseType: string;
}
