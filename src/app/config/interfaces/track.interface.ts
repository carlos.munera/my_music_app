export interface TrackInterface {
  artists: {
    name: string;
  }[];
  discNumber: number;
  durationMs: number;
  id: string;
  name: string;
  trackNumber: number;
}
