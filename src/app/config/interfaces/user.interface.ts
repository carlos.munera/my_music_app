export interface UserInterface {
  country: string;
  displayName: string;
  email: string;
  id: string;
  images: {
    url: string;
  }[];
}

export interface PrefixAuthKey {
  [key: string]: Record<string, string>;
}
