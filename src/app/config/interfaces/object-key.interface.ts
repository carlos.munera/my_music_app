export interface ObjectKeyInterface {
  [key: string]: Record<string, string>;
}
