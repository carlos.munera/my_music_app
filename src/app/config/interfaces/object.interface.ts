export interface ObjectInterface {
  [key: string]: string | boolean;
}
