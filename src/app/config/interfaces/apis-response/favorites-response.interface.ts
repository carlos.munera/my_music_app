import { AlbumResponseInterface } from './album-response.interface';

export interface FavoriteResponseInterface {
  items: {
    album: AlbumResponseInterface[];
  };
}
