import { AlbumResponseInterface } from './album-response.interface';

export interface HomeResponseInterface {
  albums: {
    items: AlbumResponseInterface[];
  };
}
