import { TrackResponseInterface } from './track-response.interface';

export interface AlbumResponseInterface {
  album_type: string;
  artists: {
    external_urls: {
      spotify: string;
    };
    href: string;
    id: string;
    name: string;
    type: string;
    uri: string;
  }[];
  available_markets: string[];
  copyrights: {
    text: string;
    type: string;
  }[];
  external_ids: {
    upc: string;
  };
  external_urls: {
    spotify: string;
  };
  genres: [];
  href: string;
  id: string;
  images: {
    height: string;
    url: string;
    width: string;
  }[];
  label: string;
  name: string;
  popularity: number;
  release_date: string;
  release_date_precision: string;
  total_tracks: number;
  tracks: {
    href: string;
    items: TrackResponseInterface[];
    limit: number;
    next: number;
    offset: number;
    previous: number;
    total: number;
  };
  type: string;
  uri: string;
}
