import { TrackInterface } from './track.interface';

export interface AlbumInterface {
  artists: {
    name: string;
  }[];
  id: string;
  images: {
    url: string;
  }[];
  name: string;
  totalTracks: number;
  tracks?: {
    items: TrackInterface[];
  };
  favorite: boolean;
}
