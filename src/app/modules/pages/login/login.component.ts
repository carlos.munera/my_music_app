import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlbumApi } from 'src/app/apis/album.api';
import { AuthApi } from 'src/app/apis/token.api';
import { UserApi } from 'src/app/apis/user.api';
import { ApiRoutes } from 'src/app/config/constants/api-routes';
import { TokenInterface } from 'src/app/config/interfaces/token.interface';
import { getAlbumList } from '@ngrxStore/state/albums/albums.actions';
import { getUserProfile } from '@ngrxStore/state/user/user.actions';
import { GlobalService } from '@src/app/services/global.service';
import { environment } from 'src/environments/environment';
import { AuthService } from '@src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public routeCode = undefined;
  private env = environment;
  public showModal = false;

  constructor(
    private activateRoute: ActivatedRoute,
    private globalService: GlobalService,
    private authService: AuthService,
    private authApi: AuthApi,
    private userApi: UserApi,
    private albumApi: AlbumApi,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.initializeApp();
  }

  public async initializeApp() {
    this.activateRoute.queryParams.subscribe((params) => {
      this.routeCode = params['code'];
    });

    if (this.routeCode) {
      await this.getToken(this.routeCode);
    }
  }

  public async getToken(routeCode: string) {
    try {
      const tokenParams = new URLSearchParams(
        Object.entries({
          code: routeCode,
          redirect_uri: this.env.requestHeader.REDIRECT_URI,
          grant_type: 'authorization_code',
        })
      ).toString();

      const authToken = Buffer.from(
        this.env.requestHeader.CLIENT_ID +
          ':' +
          this.env.requestHeader.CLIENT_SECRET
      ).toString('base64');

      this.showModal = true;

      await this.authApi
        .getToken(tokenParams, authToken)
        .then(async (token: TokenInterface) => {
          await this.getUserProfile();
          await this.getAlbums();

          this.globalService.goTo('home');
        });
    } catch (error) {
      console.log(error);
    } finally {
      this.showModal = false;
    }
  }

  public spotifyLogin() {
    const scope = this.env.requestHeader.SCOPE;
    const authorizeQryParam = this.globalService.objectToQueryParams({
      response_type: 'code',
      client_id: this.env.requestHeader.CLIENT_ID,
      scope: scope,
      redirect_uri: this.env.requestHeader.REDIRECT_URI,
      state: this.globalService.generateRandomString(16),
    });

    window.location.replace(
      this.env.host.MUSIC_APP_AUTH +
        ApiRoutes.AUTHORIZE +
        `?${authorizeQryParam}`
    );
  }

  public async getUserProfile() {
    await this.userApi.getProfile().then((userProfile) => {
      this.globalService.store.dispatch(getUserProfile({ userProfile }));
    });
  }

  public async getAlbums() {
    await this.albumApi.getAlbum(false).then((albumList) => {
      this.globalService.store.dispatch(getAlbumList({ albumList }));
    });
  }
}
