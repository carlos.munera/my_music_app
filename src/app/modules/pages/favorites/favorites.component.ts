import { Component, OnInit } from '@angular/core';
import { AlbumInterface } from '@config/interfaces/album.interface';
import { GlobalService } from '@services/global.service';
import { selectFavoriteAlbums } from '@ngrxStore/state/albums/albums.selectors';
import { Observable} from 'rxjs';
import { FavoritesConfig } from './config';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss'],
})
export class FavoritesComponent implements OnInit {
  albums$ = new Observable<Readonly<AlbumInterface[]>>;

  public config = FavoritesConfig;

  constructor(private globalService: GlobalService) {}

  ngOnInit(): void {
    this.getFavoriteAlbums();
  }

  public getFavoriteAlbums(): void {
    this.albums$ = this.globalService.store.select(selectFavoriteAlbums);
  }

  public toggleFavorite(albumId: string) {
    this.globalService.toggleFavorite(albumId);
  }
}
