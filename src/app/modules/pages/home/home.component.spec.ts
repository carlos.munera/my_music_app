import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HomeComponent } from './home.component';
import { GlobalService } from '@services/global.service';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { AlbumInterface } from '@config/interfaces/album.interface';
import { of } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '@src/app/ngrx/state/app-state.interface';

const albumList: AlbumInterface[] = [
  {
    artists: [{ name: 'Jhon Jairo Carmona' }],
    id: '1111111',
    images: [{ url: 'src/images/jhon_jairo_.svg' }],
    name: 'Viejitas pero Bunenas',
    totalTracks: 5,
    favorite: false,
  },
  {
    artists: [{ name: 'Carlos Múnera' }],
    id: '222222',
    images: [{ url: 'src/images/carlos_munera_.svg' }],
    name: 'Pop 7000',
    totalTracks: 12,
    favorite: true,
  },
];

const globalServiceMock = {
  getSelectorList: () => of(albumList),
};

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],

      declarations: [HomeComponent],

      providers: [
        { provide: GlobalService, useValue: globalServiceMock },
        Store<AppState>
      ],

      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(async () => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('getSelectorList get Albums list From the Observable', () => {
    const globalService = fixture.debugElement.injector.get(GlobalService);

    component.getAlbums();

    component.albums$.subscribe(albums => {
      expect(albums.length).toBeGreaterThan(0);
      expect(albums).toEqual(albumList);
    });
  });
});
