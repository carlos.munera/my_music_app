import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AlbumInterface } from '@config/interfaces/album.interface';
import { GlobalService } from '@services/global.service';
import { selectAlbums} from '@ngrxStore/state/albums/albums.selectors';
import { HomeConfig } from './config';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  albums$ = new Observable<Readonly<AlbumInterface[]>>;

  public config = HomeConfig;

  private onChange!: Function;

  constructor(
    private readonly globalService: GlobalService
  ) { }

  ngOnInit(): void {
    this.getAlbums();
  }

  public getAlbums(): void {
    this.albums$ = this.globalService.getSelectorList<AlbumInterface>(selectAlbums);
  }

  public toggleFavorite(albumId: string) {
    this.globalService.toggleFavorite(albumId);
  }
}
