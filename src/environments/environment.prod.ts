export const environment = {
  production: true,
  host: {
    MUSIC_APP_AUTH: 'https://accounts.spotify.com',
    MUSIC_APP_API: 'https://api.spotify.com',
  },
  requestHeader: {
    SCOPE:
      'user-read-private user-read-email user-library-modify user-library-read',
    CLIENT_ID: '28a18d2a3bbf4538b1f6085f1ae8d7d3',
    CLIENT_SECRET: '35cf8f773858417b8004ea1309b8463b',
    REDIRECT_URI: 'http://localhost:4200/',
  },
};
