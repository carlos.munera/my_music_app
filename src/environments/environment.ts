// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  host: {
    MUSIC_APP_AUTH: 'https://accounts.spotify.com',
    MUSIC_APP_API: 'https://api.spotify.com',
  },
  requestHeader: {
    SCOPE:
      'user-read-private user-read-email user-library-modify user-library-read',
    CLIENT_ID: '28a18d2a3bbf4538b1f6085f1ae8d7d3',
    CLIENT_SECRET: '443553734cd14fb29b4a6b9c786c17c7',
    REDIRECT_URI: 'http://localhost:4200/',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
