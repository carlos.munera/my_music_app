module.exports = {
  moduleNameMapper: {
    "@core/(.*)": "<rootDir>/src/app/core/$1",
    "@services/(.*)": "<rootDir>/src/app/services/$1",
    "@apis/(.*)": "<rootDir>/src/app/apis/$1",
    "@config/(.*)": "<rootDir>/src/app/config/$1",
    "@shared/(.*)": "<rootDir>/src/app/shared/$1",
    "@ngrxStore/(.*)": "<rootDir>/src/app/ngrx/$1",
  },
  preset: "jest-preset-angular",
  setupFilesAfterEnv: ["<rootDir>/setup-jest.ts"],
  collectCoverage: true,
  coverageDirectory: "coverage/my_music_app",
};
